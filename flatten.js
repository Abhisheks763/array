
const flatten = (arr) => {
        let output = []
        
        for (let i = 0; i < arr.length; i++) {
            
            if (typeof (arr[i]) == 'object') {
                output.push(...flatten(arr[i]))
            } else {
                (output.push(arr[i]))
                }
            }
            return output
}

module.exports=flatten

