const reduce2 = (elements, cb, startingValue) => {
    var i;
    if (startingValue) {
        i = 0
    } else {
        i = 1;
        startingValue = elements[0]
    }
    total = startingValue;

    for (let a = i; a < elements.length; a++) {
        total = cb(total, elements[a])

    }
    return total
}
module.exports = reduce2