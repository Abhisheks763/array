const filter2 = (elements, cb) => {
    newArray = []
    for (let i = 0; i < elements.length; i++) {

        if (cb(elements[i])) {
            newArray.push(elements[i])
        }

    }
    return newArray
}

module.exports = filter2;